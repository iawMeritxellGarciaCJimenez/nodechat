//Load app dependencies
var express = require('express');
var mongoose = require('mongoose');
const { isObject } = require('util');
var app = express();
const messageController = require('./controller/MessageController.js');

const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;

mongoose.connect(
  `mongodb://root:pass12345@localhost:27017/tutorial?authSource=admin`,
  { useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
  }
);


const index = require('./routes/index');
const path = __dirname + '/views/';

app.set('view engine', 'pug');
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path));

app.use('/', index);

/* Son els sockets, serveixen per a escoltar a les sales i enviar els missatges a tots els usuaris connectats en temps real */

io.on("connection", (socket) => {
  socket.on('sala1', msg => {
    console.log(msg);
    messageController.newMessage(msg);
    io.emit('sala1', msg);
  });
});

io.on("connection", (socket) => {
  socket.on('sala2', msg => {
    console.log(msg);
    messageController.newMessage(msg);
    io.emit('sala2', msg);
  });
});

io.on("connection", (socket) => {
  socket.on('sala3', msg => {
    console.log(msg);
    messageController.newMessage(msg);
    io.emit('sala3', msg);
  });
});

io.on("connection", (socket) => {
  socket.on('sala4', msg => {
    console.log(msg);
    messageController.newMessage(msg);
    io.emit('sala4', msg);
  });
});

http.listen(port, () => {
  console.log(`Socket.IO server running at http://localhost:${port}/`);
});