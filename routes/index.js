const express = require('express');
const router = express.Router();
const path = require('path');
let cookieParser = require('cookie-parser');
const UserController = require('../controller/UserController.js');
router.use(cookieParser());

//Middleware para mostrar datos del request
router.use (function (req,res,next) {
  console.log('/' + req.method);
  next();
});

/* Anem a la pantalla d'iniciar sessió o anar a registre */
router.get('/',function(req,res){
  res.sendFile(path.resolve('views/index.html'));
});

/* Cridem a la funció login del controlador */
router.post('/user/login', UserController.login);
/* Aquest és el següent pas en cas de voler iniciar sessió i existeixi l'usuari que vol fer l'acció.
    Es crea una cookie per a l'usuari */
router.post('/user/login', function(req, res) {
  res.cookie("username", req.body.name);
  res.redirect('/chat/list');
});

/* Anem a la pantalla de registre */
router.get('/user/register', function(req,res){
  res.sendFile(path.resolve('views/register.html'));
});


/* Cridem a la funció de registre del controlador */
router.post('/register', UserController.newUser);

/* Anem a la pantalla de llista de sales de chat */
router.get('/chat/list',function(req,res){
  res.sendFile(path.resolve('views/listaChat.html'));
});

/* Anem a la pantalla de la sala indicada, li enviem el nom de l'usuari que està entrant i el numero de la sala */
router.post('/chat/view',function(req,res){
  res.render('chat.pug', {username: req.cookies, numSala: req.body.sala});
});

module.exports = router;
