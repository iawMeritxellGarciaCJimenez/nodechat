var User = require('../models/user');

/* Comprobem que l'usuari que es vol crear no existeix i si no existeix el creem, si existeix no es crea */
var newUser = (req, res, next) => {
  User.findOne({name: req.body.name, password: req.body.password}, function(err, user) {
    if (user == undefined) {
      var user = new User({name: req.body.name, password: req.body.password});
      user.save();
      res.redirect('/');  
    } else {
      res.redirect('/');
    }
  });
};

/* Comprobem que l'usuari que vol iniciar sessió existeixi, si existeix pasarem al següent pas */
var login = async (req, res, next) => {

  User.findOne({name: req.body.name, password: req.body.password}, function(err, user) {
    if (user != undefined) {
      //document.cookie = "username=" + req.body.name;
      //res.redirect('/chat/list');
      next();
    } else {
      res.redirect('/');
    }
  });
};

module.exports = {newUser, login};