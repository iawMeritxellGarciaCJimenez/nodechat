var Message = require('../models/message');

async function newMessage (msg) {
    var newmsg = new Message({msg: msg});
    await newmsg.save();
    return newmsg;
};

module.exports = {newMessage};