var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var messageSchema = new Schema({
    msg: String
});

module.exports = mongoose.model('Message', messageSchema);